syn on
set incsearch
set tabstop=4
set showmatch
set autoindent
set nocindent
set backspace=2
