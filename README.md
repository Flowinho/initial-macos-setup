# Initial macOS Setup

This repository hosts one or multiple scripts that quickly set up a freshly installed macOS machine. This automates application installation and some operating system settings.