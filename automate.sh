#!/usr/bin/env bash
# Setup script for setting up a new macos machineecho "Starting setup"# install xcode CLI
xcode-select --install

# Check for Homebrew to be present, install if it's missing
if test ! $(which brew); then
    echo "Installing homebrew..."
    ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
fi

# Update shallow clones once to reduce load on github servers (required by homebrew)
git -C /usr/local/Homebrew/Library/Taps/homebrew/homebrew-core fetch --unshallow
git -C /usr/local/Homebrew/Library/Taps/homebrew/homebrew-cask fetch --unshallow

# Update homebrew recipes
brew update

# Define formulaes to be installed
PACKAGES=(
    git 			# because macOS ships with outdated git
    ruby			# because macOS ships with outdated ruby
    vim				# because vim
	carthage		# iOS development dependency
	ffmpeg			# video-converter, useful for youtube-dl
	gnupg			# basic encryption
	htop			# show system-usage in terminal
	p7zip			# strong compression if needed
	python			# dependency for software that might be installed in the future
	wget			# terminal downloader
	youtube-dl		# video-downloader
)

echo "Installing formuales.."
brew install ${PACKAGES[@]}

echo "Cleaning up"
brew cleanup

# Setup ruby gems
echo "Installing ruby gems..."
RUBY_GEMS=(
	bundler			# Fast gem-installation
	jekyll			# blogging-engine i use
)

# Installing casks
CASKS=(
	iterm2					# best terminal emulator for macOS
	adobe-acrobat-reader	# sadly, best pdf-viewer for macos
	visual-studio-code		# code-editor
	macdown					# markdown-editor
	devonthink				# document-manager
	discord					# chatsoftware
	slack					# chatsoftware
	doxie					# software used for doxie-scanners
	imazing					# best tool to manage iOS devices
	bitwarden				# password-manager
	postman					# API client
	signal					# secure messenger
	marsedit				# blog-editor
	notion					# collaborative workspace tool
	firefox					# browser
	omnifocus				# taskmanager
	omnidisksweeper			# diskspace-analyzer
	craft					# notes
	balsamiq-wireframes		# prototypes for apps
	appcleaner				# uninstall apps the right way
	dozer					# configure mac menu icons
	stats					# show system-stats in the menu bar
	fantastical				# calendar-software
	alfred3					# spotlight-replacement
	cyberduck				# remote-storage tool
	keka					# archival tool
	netnewswire				# news-reader
	gpg-suite				# encryption tool
)
echo "Installing casks..."
brew install --cask ${CASKS[@]}

# Import PGP keys
echo "Import pgp-keys..."
curl https://gitlab.com/Flowinho/pgp-keys/-/raw/master/ahri.asc > ahri.asc
gpg --import ahri.asc

# Additional OS configuration

## Tell macOS to instantly reboot after a freeze
sudo systemsetup -setrestartfreeze on

## Check for software-updates daily (instead of weekly)
defaults write com.apple.SoftwareUpdate ScheduleFrequency -int 1

## Show all hidden files
defaults write com.apple.finder AppleShowAllFiles -bool true

## Expand macOS Open / Save / Print dialogues by default
defaults write NSGlobalDomain NSNavPanelExpandedStateForSaveMode -bool true
defaults write NSGlobalDomain NSNavPanelExpandedStateForSaveMode2 -bool true

## Disable natural scrolling
defaults write NSGlobalDomain com.apple.swipescrolldirection -bool false

## Disable auto-correction
defaults write NSGlobalDomain NSAutomaticSpellingCorrectionEnabled -bool false

## Enable tap to click
defaults write com.apple.driver.AppleBluetoothMultitouch.trackpad Clicking -bool true
defaults -currentHost write NSGlobalDomain com.apple.mouse.tapBehavior -int 1

## Require password as soon as screensaver or sleep mode starts
defaults write com.apple.screensaver askForPassword -int 1
defaults write com.apple.screensaver askForPasswordDelay -int 0

## Show filename extensions by default
defaults write NSGlobalDomain AppleShowAllExtensions -bool true

## Purge default apps from the dock
defaults delete com.apple.dock persistent-apps
defaults delete com.apple.dock persistent-others

## Add four separators to the dock
#defaults write com.apple.dock persistent-apps -array-add '{"tile-type"="spacer-tile";}';
#defaults write com.apple.dock persistent-apps -array-add '{"tile-type"="spacer-tile";}';
#defaults write com.apple.dock persistent-apps -array-add '{"tile-type"="spacer-tile";}';
#defaults write com.apple.dock persistent-apps -array-add '{"tile-type"="spacer-tile";}';

## Transform the dock to show only running applications
defaults write com.apple.dock static-only -bool true;

## Place dock on the left of the screen
defaults write com.apple.Dock orientation left

## Apply all changes to macOS dock configuration
killall Dock

# Configure the terminal
echo "Configure terminal aliases..."

alias weatherForecastLudwigsburg='curl http://wttr.in/Ludwigsburg'

# Mac Control
alias power_off='sudo /sbin/shutdown -h now'

# macOS Control
alias show_hidden_files='defaults write com.apple.finder AppleShowAllFiles YES && killall Finder'
alias hide_hidden_files='defaults write com.apple.finder AppleShowAllFiles NO && killall Finder'

# Bash Control
#alias reload_profile='. ~/.profile'

# General directory navigation
alias cd..='cd ..'
alias ..='cd ..'
alias ls='ls -G'
alias l='ls'
alias ll='ls -lh'
alias la='ls -lha'

# Web
alias lengthenURL='read -p "URL to be deshortened: " answer && curl -sIL $answer | grep ^Location;'

# Custom directory navigation
alias goto_rep='cd ~/Repositories && ls -ll'

# Application Control
# Archiving
alias tarball='tar -cvf' # outputfilename + directory to tar
alias tarballCompressed='tar cfvz'
alias tarballUnpack='tar xfvz'
alias sync='rsync -azvhP'
alias downloadYoutubePlaylistMP3='youtube-dl --ignore-errors -x --audio-format "mp3" --embed-thumbnail --audio-quality 0 --metadata-from-title "(?P<artist>.+?) - (?P<title>.+)" --restrict-filenames -o '"'"'%(playlist)s/%(playlist_index)s - %(title)s.%(ext)s'"'"''
alias downloadYoutubePlaylistVideo='youtube-dl --ignore-errors  --restrict-filenames -o '"'"'%(playlist)s/%(playlist_index)s - %(title)s.%(ext)s'"'"''

# VPN
alias enableVpnOnly='sudo ufw reset && sudo ufw default deny incoming && sudo ufw default deny outgoing && sudo ufw allow out on tun0 from any to any && sudo ufw enable'
alias disableVpnOnly='sudo ufw reset && sudo ufw default deny incoming && sudo ufw default allow outgoing && sudo ufw enable'

# Encryption
alias encryptFile='gpg --symmetric --cipher-algo aes256 --digest-algo sha256 --compress-algo none -z 0 --armour' # file name need to be specified
alias registerSSH='ssh-add -K ~/.ssh/id_rsa'

# Drive control
alias ramdiskMacOS='read -p "GB size for RAMdisk: " answer && sudo diskutil erasevolume HFS+ "ramdisk" `hdiutil attach -nomount ram://$(( $answer * 2097152))`'

# File cleanup
alias findExt='read -p "Extension to be searched: " answer && find . -name "*.$answer" -type f'
alias removeExt='read -p "Extension to be purged: " answer && find . -name "*.$answer" -type f -delete'

# GIT Command substitution
alias gBranch='git branch -a -v -v'
alias gFetch='git fetch && git branch -a -v -v'
alias gTree='git log --graph --oneline'
alias gTreeFancy='git log --graph --pretty=format:"%C(yellow)%h %Cred%ad %Cblue%an %Cgreen%d %Creset%s" --date=short'
alias gListTags='git log --date-order --graph --tags --simplify-by-decoration --pretty=format:"%ai %h %d"'
alias gCommitAll='git add . && git commit'
alias gCleanUp='git fetch -p && git branch -vv | awk '"'"'/: gone]/{print $1}'"'"' | xargs git branch -d'
alias gForcePush='currentBranch=$(git branch | sed -n -e '"'"'s/^\* \(.*\)/\1/p'"'"') && echo "$currentBranch will be forced pushed to origin" && git push -f origin $currentBranch'
alias gWorkspace='osascript -e "tell application \"Terminal\"" -e "tell application \"System Events\" to tell process \"Terminal\" to keystroke \"n\" using command down" -e "do script with command \"cd `pwd`;clear && gFetch\" in selected tab of the front window" -e "end tell" &> /dev/null && osascript -e "tell application \"Terminal\"" -e "tell application \"System Events\" to tell process \"Terminal\" to keystroke \"n\" using command down" -e "do script with command \"cd `pwd`;clear && gTree\" in selected tab of the front window" -e "end tell" &> /dev/null'

# Symlink vimrc
ln -s .vimrc ~/.vimrc

echo "Setup completed!"
